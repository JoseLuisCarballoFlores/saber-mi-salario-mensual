
#include <stdio.h>
#include <stdlib.h>

int main(){
	
	float horas, euros_hora, total_mes;
	
printf("¿Cuantas horas has trabajado este mes? ");
scanf("%f", &horas);

printf("¿Cuanto te pagan la hora? ");
scanf("%f", &euros_hora);

total_mes = horas * euros_hora;

printf("El total de tu salario es %.2f\n\n", total_mes); //el .2f significa que quiero que solo me salgan 2 decimales.

return 0;
}
